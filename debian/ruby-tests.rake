require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = FileList['./spec/**/*_spec.rb'] - FileList['./spec/unit/s3_saver_spec.rb','./spec/feature/minitest_spec.rb','./spec/feature/testunit_spec.rb','./spec/spinach/spinach_spec.rb']
end
